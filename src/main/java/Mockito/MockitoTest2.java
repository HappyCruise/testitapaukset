package Mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.mockito.Captor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
public class MockitoTest2 {
	@Mock
	IHinnoittelija hinnoittelijaMock;
	
	float alkusaldo = 100.0f;
	Asiakas asiakas;
	float alennus;
	
	@BeforeEach
	public void setup() {
		
		MockitoAnnotations.openMocks(this);
		asiakas = new Asiakas(alkusaldo);
		alennus = 20.0f;
	}
	
	@ParameterizedTest
	@CsvSource({"50", "100", "110"})
	public void TestMockitoKäsittelijä(float listahinta) {
		//Arrange
		Tuote tuote = new Tuote("TDD in Action", listahinta);
		
		//Record
		doNothing().when(hinnoittelijaMock).aloita();
		when(hinnoittelijaMock.getAlennusProsentti(asiakas, tuote)).thenReturn(alennus);

		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();

				alennus = (float) args[1];
				return null;
			}
		}).when(hinnoittelijaMock).setAlennusProsentti(any(), anyFloat());

		
		when(hinnoittelijaMock.getAlennusProsentti(asiakas, tuote)).thenReturn(alennus);
		doNothing().when(hinnoittelijaMock).lopeta();
		
		//Act
		TilaustenKäsittely käsittelijä = new TilaustenKäsittely();
		käsittelijä.setHinnoittelija(hinnoittelijaMock);
		käsittelijä.käsittele(new Tilaus(asiakas, tuote));

		float loppusaldo = alkusaldo - (listahinta * (1 - hinnoittelijaMock.getAlennusProsentti(asiakas, tuote) / 100));
		assertEquals(asiakas.getSaldo(), loppusaldo);
		
	}
}
