package Mockito;

public interface IHinnoittelija {
	public void aloita();
	public abstract float getAlennusProsentti(Asiakas asiakas, Tuote tuote);
	public void setAlennusProsentti(Asiakas asiakas, float prosentti);
	public void lopeta();
}
