package org.example;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeTest {

    @ParameterizedTest(name="Sekunnit {0} tunteina: {1}")
    @CsvSource({"0, 0:00:00", "1, 0:00:01", "60, 0:01:00", "3600, 1:00:00","36000, 10:00:00","3705, 1:01:45", "86400, 24:00:00", "86401, 24:00:01", "-3600, -1:00:00"})
    public void testTime(int sekunnit, String tunnit){

        String tulos = TimeUtils.secToTime(sekunnit);
        String failTxt  ="Oletettu tulos: " + tunnit + " Tulos: " + tulos;
        assertEquals(tulos, tunnit, failTxt);
    }
}
